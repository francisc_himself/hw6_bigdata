#!/usr/bin/python
from __builtin__ import enumerate

import dummyMapReduce as mr
import sqlite3
import argparse
import sys
import random
import hashlib
from pprint import pprint


name_regular_db = "features_fine.db"
name_inverted_db = "inverted_features_fine.db"
name_lsh_db = "lsh.db"

#### GLOBALS ####
InvertedIndex = dict()
ForwardIndex = dict()
DOCUMENTS = []
m = 2147483647
b = 30
r =  5
A = [random.randint(1, m) for i in range (b*r)]
B = [random.randint(1, m) for i in range (b*r)]
BandGroups = {}
dict_lsh = dict()
counter_dist_inverted = 0
counter_dist_lsh = 0
#### GLOBALS ####

class MapReduceHelper(mr.MapReduce):
    def map(self, docId, docContent):
        for element in docContent:
            self.emit(element, docId)

    def reduce(self, element, documents):
        InvertedIndex[element] = documents


#--#--#--#--#--#--#--#--#--#--#--#--
def sequel_blob_to_list(blob):
    the_list = str(blob).split('\x0a')[:-2]
    return the_list
# --#--#--#--#--#--#--#--#--#--#--#--

# --#--#--#--#--#--#--#--#--#--#--#--
def list_to_sequel_blob(a_list):
    str_blob_to_persist = ""
    for an_elem in a_list:
        str_blob_to_persist += str(an_elem)+"\n"
    return str_blob_to_persist
# --#--#--#--#--#--#--#--#--#--#--#--

# --#--#--#--#--#--#--#--#--#--#--#--
def distance(set_a, set_b):
    return 1 - float(len(set_a & set_b)) / len(set_a | set_b)
# --#--#--#--#--#--#--#--#--#--#--#--

# --#--#--#--#--#--#--#--#--#--#--#--
def search_inv(counter_dist_inverted, content, ID, threshold):
    candidates = set()
    for element in content:
        if element in InvertedIndex:
            # Check ID:

            # Perform union
            candidates = candidates | set(InvertedIndex[element])
    for docId in candidates:
        docContent = ForwardIndex[docId]
        # print "distance =", distance(set(content), set(docContent))
        # about to calculate distance:
        counter_dist_inverted += 1
        if distance(set(content), set(docContent)) <= threshold:
            print "DOCID=", docId
    print "INVERTED PERFORMED:", counter_dist_inverted, "DISTANCES"
            # yield docId
# --#--#--#--#--#--#--#--#--#--#--#--


# --#--#--#--#--#--#--#--#--#--#--#--
def init_hash():
    a_hash = hashlib.md5()
    return a_hash
# --#--#--#--#--#--#--#--#--#--#--#--


# --#--#--#--#--#--#--#--#--#--#--#--
def minhash(contents, band, row):
    aa = A[band*r+row]
    bb = B[band*r+row]
    perm = [(aa*int(x)+bb) % m for x in contents]
    return min(perm)
# --#--#--#--#--#--#--#--#--#--#--#--



# --#--#--#--#--#--#--#--#--#--#--#--
def computeBands(content):
    # print "Computing bands for data length", len(content)
    bands = []
    for band in range(b):
        H = init_hash()
        for row in range(r):
            H.update(str(minhash(content, band, row)))
        bands.append(H.hexdigest())
    return bands
# --#--#--#--#--#--#--#--#--#--#--#--



# --#--#--#--#--#--#--#--#--#--#--#--
def computeBandGroups():
    BandGroups = [{} for _i in range(b)]
    for docId in ForwardIndex:
        its_bands = dict_lsh[docId]
        for band_idx, Hash in enumerate(its_bands):
            if (Hash) in BandGroups[band_idx]:
                BandGroups[band_idx][Hash].add(docId)
            else:
                BandGroups[band_idx][Hash]={docId}
    return BandGroups
# --#--#--#--#--#--#--#--#--#--#--#--



# --#--#--#--#--#--#--#--#--#--#--#--
def search_lsh(counter_dist_lsh, BandGroups, docId, content, threshold):
    print "In funct search_lsh"
    candidates = set()
    bands = dict_lsh[docId]
    # print "Your bands:"
    # pprint(bands)
    for band_index, H in enumerate(bands):
        # print "band_index=", band_index, "band=", bands[band_index], H
        # print "H=", H
        # pprint(BandGroups)
        if H in BandGroups[band_index]:
            # Perform union
            # print "ok"
            candidates = candidates | set(BandGroups[band_index][H])
    for docId in candidates:
        docContent = ForwardIndex[docId]
        # about to calculated distance!
        counter_dist_lsh += 1
        if distance(set(content), set(docContent)) <= threshold:
            print docId, distance(set(content), set(docContent))
    print "search_lsh PERFORMED", counter_dist_lsh, "DISTANCE CALCULATIONS"
    #         # yield docId
# --#--#--#--#--#--#--#--#--#--#--#--



# # # # # # #
def main():
    parser = argparse.ArgumentParser(description="lab BIG data")
    parser.add_argument("-mkinvertedfine", action='store_true', help="SQL:Create inverted DB", required=False)
    parser.add_argument("-search_inverted", action='store_true', help="Search unsing InvertedIndex", required=False)
    parser.add_argument("-lsh", action='store_true', help="SQL: Create Locality Sensitive Hashing DB", required=False)
    parser.add_argument("-search_lsh", action='store_true', help="Search using LSH", required=False)
    args = parser.parse_args()
    if len(sys.argv[1:]) == 0:
        parser.print_help()

    # TASK 2 # # TASK 2 # # TASK 2 # # TASK 2 # # TASK 2 # # TASK 2 # # TASK 2 # # TASK 2 # # TASK 2 # # TASK 2 #
    if args.mkinvertedfine:
        # Instantiate MapReduce class
        mrh = MapReduceHelper()
        # gather all from previous database
        with sqlite3.connect(name_regular_db) as conn_regular:
            # The dict containing all data from features_fine
            dict_get_all_from_db_regular = dict()
            dict_hash_grams = dict()
            cursor = conn_regular.cursor()
            query_select_all = "SELECT * from Homeworks"
            cursor.execute(query_select_all)
            for a_row in cursor:
                a_hash = a_row[0]
                an_assign = a_row[1]
                a_student = a_row[2]
                an_ngramset = sequel_blob_to_list(a_row[3])
                dict_get_all_from_db_regular[a_hash] = [an_assign, a_student, an_ngramset]
                dict_hash_grams[a_hash] = an_ngramset
                DOCUMENTS.append([a_hash, an_ngramset])
        # Perform map-reduce
        mrh.run(DOCUMENTS)

        with sqlite3.connect(name_inverted_db) as conn_inverted:
            create_inverted_hw_db = """CREATE TABLE IF NOT EXISTS Inverted_Homeworks(
                                          Ngram string PRIMARY KEY,
                                          Containers blob
                                          );"""
            a_cursor = conn_inverted.cursor()
            a_cursor.execute(create_inverted_hw_db)
            query_insert_inverted_db = '''INSERT INTO Inverted_Homeworks(Ngram, Containers) values\
                                                            (?, ?)'''
            for idx, key in enumerate(InvertedIndex.keys()):
                print "ngram", idx, " out of ", len(InvertedIndex)

                str_for_blob = list_to_sequel_blob(InvertedIndex[key])
                conn_inverted.execute(query_insert_inverted_db, [key, buffer(str_for_blob)])
                conn_inverted.commit()
        print "Done."
        # TASK 2 # # TASK 2 # # TASK 2 # # TASK 2 # # TASK 2 # # TASK 2 # # TASK 2 # # TASK 2 # # TASK 2 # # TASK 2 #


    # TASK 3 # # TASK 3 # # TASK 3 # # TASK 3 # # TASK 3 # # TASK 3 # # TASK 3 # # TASK 3 # # TASK 3 # # TASK 3 #
    if args.search_inverted:
        print "SEARCH INVERTED"

        # gather all from previous database
        with sqlite3.connect(name_regular_db) as conn_regular:
            cursor = conn_regular.cursor()
            query_select_all = "SELECT * from Homeworks"
            cursor.execute(query_select_all)
            for a_row in cursor:
                a_hash = a_row[0]
                an_assign = a_row[1]
                a_student = a_row[2]
                an_ngramset = sequel_blob_to_list(a_row[3])
                ForwardIndex[a_hash] = an_ngramset
                DOCUMENTS.append([a_hash, an_ngramset])

        # Below is filled the InvertedIndex dictionary
        mrh = MapReduceHelper()
        mrh.run(DOCUMENTS)

        print len(InvertedIndex)
        print len(ForwardIndex)

        # From here, InvertedIndex and ForwardIndex are available!
        search_inv(counter_dist_inverted, DOCUMENTS[0][1], DOCUMENTS[0][0], 0.9)
    # TASK 3 # # TASK 3 # # TASK 3 # # TASK 3 # # TASK 3 # # TASK 3 # # TASK 3 # # TASK 3 # # TASK 3 # # TASK 3 #


    # TASK 4 # # TASK 4 # # TASK 4 # # TASK 4 # # TASK 4 # # TASK 4 # # TASK 4 # # TASK 4 # # TASK 4 # # TASK 4 #
    DOCUMENTS_fwd = []
    if args.lsh:
        # gather all from previous database
        with sqlite3.connect(name_regular_db) as conn_regular:
            cursor = conn_regular.cursor()
            query_select_all = "SELECT * from Homeworks"
            cursor.execute(query_select_all)
            for a_row in cursor:
                a_hash = a_row[0]
                an_assign = a_row[1]
                a_student = a_row[2]
                an_ngramset = sequel_blob_to_list(a_row[3])
                ForwardIndex[a_hash] = an_ngramset
                DOCUMENTS.append([a_hash, an_ngramset])

        # Below is filled the InvertedIndex dictionary
        mrh = MapReduceHelper()
        mrh.run(DOCUMENTS)

        print len(InvertedIndex)
        print len(ForwardIndex)
        # From here on, both InvertedIndex and ForwardIndex are available


        # BandGroups = computeBandGroups()
        # pprint (BandGroups)
        # print len(BandGroups)


        with sqlite3.connect(name_lsh_db) as conn_lsh:
            cursor = conn_lsh.cursor()

            #########first query in many-columned db########################################
            query_create_lsh_table_part = "CREATE TABLE IF NOT EXISTS data ( Hash string PRIMARY KEY, "
            # 30 columns now to be added:
            for idx in range(b-1):
                a_part = "col_"+str(idx)+" blob, "
                query_create_lsh_table_part += a_part
            a_part = "col_"+str(idx+1)+" blob "
            query_create_lsh_table_part += a_part
            query_create_lsh_table_part += ");"
            print query_create_lsh_table_part
            cursor.execute(query_create_lsh_table_part)
            ######################################################################################

            ##########second query in many-columned db#################################
            query_insert_inverted_db = "INSERT INTO data( Hash, "
            # 30 columns now to be interted:
            for idx in range(b - 1):
                a_part = "col_" + str(idx) + ", "
                query_insert_inverted_db += a_part
            a_part = "col_" + str(idx + 1) + ") values ("
            query_insert_inverted_db += a_part
            ###########################################################################


            for idx, a_hash in enumerate(ForwardIndex.keys()):
                print idx, "out of", len(ForwardIndex)
                its_bands = computeBands(ForwardIndex[a_hash])
                # pprint(its_bands)
                the_query = query_insert_inverted_db+"'"+a_hash+"',"
                for index, a_band in enumerate(its_bands):
                    the_query += "'"+str(a_band)+"',  "
                the_query = the_query[:-3]
                the_query += ")"
                print the_query

                cursor.execute(the_query)
                # Empty the query string for the next DB candidate!
                the_query = ""

        with sqlite3.connect(name_lsh_db) as conn_lsh:
                cursor = conn_lsh.cursor()
                for index in range(b):
                    cursor.execute('CREATE INDEX IF NOT EXISTS col_'+str(index)+ \
                    " ON data(col_"+str(index)+")")

    # TASK 4 # # TASK 4 # # TASK 4 # # TASK 4 # # TASK 4 # # TASK 4 # # TASK 4 # # TASK 4 # # TASK 4 # # TASK 4 #


    # TASK 5 # # TASK 5 # # TASK 5 # # TASK 5 # # TASK 5 # # TASK 5 # # TASK 5 # # TASK 5 # # TASK 5 # # TASK 5 #
    if args.search_lsh:
        with sqlite3.connect(name_lsh_db) as conn_lsh:
            query_select_all_lsh = "SELECT * from data"
            cursor = conn_lsh.cursor()
            cursor.execute(query_select_all_lsh)
            for a_row in cursor:
                hw_hash = a_row[0]
                a_list = []
                for idx in range(1, b):
                    a_list.append(a_row[idx])
                dict_lsh[hw_hash] = a_list
            print ("Added all data from lsh DB to a dict!")
            # pprint (dict_lsh)

        # Now process the data from lsh DB in dict

        ##------------------------------------------------
        # gather all from previous database
        with sqlite3.connect(name_regular_db) as conn_regular:
            cursor = conn_regular.cursor()
            query_select_all = "SELECT * from Homeworks"
            cursor.execute(query_select_all)
            for a_row in cursor:
                a_hash = a_row[0]
                an_assign = a_row[1]
                a_student = a_row[2]
                an_ngramset = sequel_blob_to_list(a_row[3])
                ForwardIndex[a_hash] = an_ngramset
                DOCUMENTS.append([a_hash, an_ngramset])

                # Below is filled the InvertedIndex dictionary
        mrh = MapReduceHelper()
        mrh.run(DOCUMENTS)

        print len(InvertedIndex)
        print len(ForwardIndex)
        ##------------------------------------------------

        # 30 band groups
        # Create the band groups, just for this homework
        # BandGroups = [{} for _i in range(b)]
        # for docId in ForwardIndex:
        #     its_bands = dict_lsh[docId]
        #     for band_idx, Hash in enumerate(its_bands):
        #         if (Hash) in BandGroups[band_idx]:
        #             BandGroups[band_idx][Hash].add(docId)
        #         else:
        #             BandGroups[band_idx][Hash]={docId}

        # Now, look for similarities with candidates and distance
        # search by content of homework and threshold
        print "bf lsh"
        # Checking below is contents ok
        # pprint(DOCUMENTS[0][1])

        BandGroups = computeBandGroups()
        # pprint(BandGroups)

        search_lsh(counter_dist_lsh, BandGroups, DOCUMENTS[0][0], DOCUMENTS[0][1], 0.9)

    # TASK 5 # # TASK 5 # # TASK 5 # # TASK 5 # # TASK 5 # # TASK 5 # # TASK 5 # # TASK 5 # # TASK 5 # # TASK 5 #
if __name__ == "__main__":
    main()



















