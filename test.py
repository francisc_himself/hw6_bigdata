import re, os
from dateutil.parser import parser

import dummyMapReduce as mr
import sqlite3
import argparse
import sys
import bisect
from pprint import pprint

rxWord = re.compile(r"[a-zA-Z]{3,}")
inverted_db_name = "inverted.db"
create_inverted_db = """CREATE TABLE IF NOT EXISTS Inverted(
                        Term string PRIMARY KEY,
                        Containers blob
                        );"""
dict_for_db = dict()
DOCUMENTS = [
    ("doc1", """MapReduce is a programming model and an associated implementation for processing and generating large
                    data sets with a parallel, distributed algorithm on a cluster."""),
    ("doc2", """Conceptually similar approaches have been very well known since 1995 with the Message Passing
                    Interface standard having reduce and scatter operations."""),
    ("doc3", """A MapReduce program is composed of a Map() procedure (method) that performs filtering and sorting
                (such as sorting students by first name into queues, one queue for each name) and a Reduce() method
                that performs a summary operation (such as counting the number of students in each queue, yielding
                name frequencies)."""),
    ("doc4", """The "MapReduce System" (also called "infrastructure" or "framework") orchestrates the processing by
                marshalling the distributed servers, running the various tasks in parallel, managing all
                communications and data transfers between the various parts of the system, and providing for
                redundancy and fault tolerance."""),
    ("doc5", """The model is inspired by the map and reduce functions commonly used in functional programming,
                although their purpose in the MapReduce framework is not the same as in their original forms."""),
    ("doc6", """The key contributions of the MapReduce framework are not the actual map and reduce functions, but
                the scalability and fault-tolerance achieved for a variety of applications by optimizing the
                execution engine once"""),
    ("doc8", """As such, a single-threaded implementation of MapReduce will usually not be faster than a traditional
                (non-MapReduce) implementation, any gains are usually only seen with multi-threaded implementations"""),
    ("doc9", """The use of this model is beneficial only when the optimized distributed shuffle operation
                (which reduces network communication cost) and fault tolerance features of the MapReduce framework
                come into play."""),
    ("doc10", """Optimizing the communication cost is essential to a good MapReduce algorithm."""),
    ("doc11", "balaur bou balaur balaur balaur balaur")
]


class WordCounter(mr.MapReduce):

    def map(self, _fileName, content):
        # print " Current document number: ", _fileName
        my_dict = dict()
        words = rxWord.findall(content)
        for idx, word in enumerate(words):
            if word not in my_dict or word.lower() not in my_dict:
                my_dict[word.lower()] = 1
            else:
                my_dict[word.lower()]+= 1
        for a_word in my_dict:
            # print "     emiting:::", a_word.lower(), "->", my_dict[a_word]
            self.emit(a_word.lower(), _fileName)#+"->"+str(my_dict[a_word]))

    def reduce(self, word, counts):
        wordCount = counts
        # print word, wordCount
        dict_for_db[word] = wordCount

# def search_similar_files(content, threshold):
#     candidates = set()
#     for element in content:
#         if element in InvertedIndex:
#             #union
#             candidates = candidates | InvertedIndex[element]
#     for docId in candidates:
#         docContent = ForwardIndex[docId]
#         if distance(content, docContent) <= threshold:
#             yield docId

count_grams = 0

######################################
def sequel_blob_to_list(blob, dict_so_far, blob_at_call_time):
    the_list = []
    the_list = str(blob).split('\x0a')
   # pprint(the_list)map
    the_list = the_list[:-2]

    the_list = the_list#[]

    for an_elem in the_list:
        if not an_elem in dict_so_far:
            dict_so_far[an_elem] = [blob_at_call_time]
        else:
            dict_so_far[an_elem].append(blob_at_call_time)
    return the_list
######################################


#########################################
def list_to_sequel_blob(a_list):
    str_blob_to_persist = ""
    for an_elem in a_list:
        str_blob_to_persist += str(an_elem) +"\n"
    return str_blob_to_persist
#########################################

###############################################
# Function that searches similar elements using the reverse index
#def search_inv():


###############################################

def main():
    parser = argparse.ArgumentParser(description="lab BIG data")
    parser.add_argument ("-mkinverteddb", action='store_true', help="SQL:Create inverted DB", required=False)
    parser.add_argument("-inverted_db_from_regular", action='store_true', help="..", required=False)
    args = parser.parse_args()
    if len(sys.argv[1:]) == 0:
        parser.print_help()

    if args.mkinverteddb:
        wc = WordCounter()
        wc.run(DOCUMENTS)

        # Connect to the database to be created for mapreduce example
        with sqlite3.connect(inverted_db_name) as conn_inverted:
            cursor = conn_inverted
            # Here, the Inverted index database gets created
            cursor.execute(create_inverted_db)

            # Query for inserting into the DB a CRC32 value along with a blob showing its containers (hashes)
            query_insert_inverted_db ='''INSERT INTO Inverted(Term, Containers) values\
                                         (?, ?)'''

            for idx, key in enumerate(dict_for_db.keys()):
                print "...key#", idx
                str_blob_to_persist = ""
                a_list = dict_for_db[key]
                str_blob_to_persist = list_to_sequel_blob(a_list)
                print "BLOB:", str_blob_to_persist

                conn_inverted.execute(query_insert_inverted_db, [ key, buffer(str_blob_to_persist) ])
                conn_inverted.commit()


    # This argument creates an inverted index DB from the features_fine.db database
    if args.inverted_db_from_regular:
        name_regular_db = "features_fine.db"
        name_inverted_db = "inverted_fine.db"

        # A dict that shall be used
        all_ngrams__dict = dict()

        with sqlite3.connect(name_regular_db) as conn_regular:
            # The dict containing absolutely all data from features_fine
            dict_get_all_from_db_regular = dict()
            cursor = conn_regular.cursor()
            query_select_all = "SELECT * from Homeworks"
            cursor.execute(query_select_all)
            for a_row in cursor:
                a_hash = a_row[0]
                an_assign = a_row[1]
                a_student = a_row[2]
                an_ngramset = sequel_blob_to_list(a_row[3], all_ngrams__dict,a_hash)

                dict_get_all_from_db_regular[a_hash] = [an_assign, a_student, an_ngramset]

        len_all_ngrams_dict = len(all_ngrams__dict)
        print len_all_ngrams_dict
        print len(dict_get_all_from_db_regular)

    with sqlite3.connect(name_inverted_db) as conn_inverted_hw:
        create_inverted_hw_db = """CREATE TABLE IF NOT EXISTS Inverted_Homeworks(
                              Ngram string PRIMARY KEY,
                              Containers blob
                              );"""
        a_cursor = conn_inverted_hw.cursor()
        a_cursor.execute(create_inverted_hw_db)
        query_insert_inverted_db = '''INSERT INTO Inverted_Homeworks(Ngram, Containers) values\
                                                (?, ?)'''
        for idx, key in enumerate(all_ngrams__dict.keys()):
            print "ngram", idx, " out of ", len_all_ngrams_dict
            str_for_blob = list_to_sequel_blob(all_ngrams__dict[key])
            conn_inverted_hw.execute(query_insert_inverted_db, [key, buffer(str_for_blob)])
            conn_inverted_hw.commit()

            #   #   # IGNORE THIS PART, it was just for checking wether I have not lost
            #   #   # data while processing it.
            #   #   # I prepare a list of hashes (we know that 946 homeworks exist)
            #   #   list_of_hashes_extracted = []
            #   #   # the dict below made me check for corectness, since I keep calling it as a mutable function
            #   #   # argument
            #   #   for an_ngram in all_ngrams__dict.keys():
            #   #       # I retrieve a list of the hashes of documents containing that ngram
            #   #       the_list = all_ngrams__dict[an_ngram]
            #   #       # check each hash if not in the list of unique ones
            #   #       for a_list_elem in the_list:
            #   #           if not a_list_elem in list_of_hashes_extracted:
            #   #               list_of_hashes_extracted.append(a_list_elem)
            #   #   print len(list_of_hashes_extracted)
            #   #   # RESULT OK, 946 homeworks resulted from reversed indexing aswell!!!


if __name__ == "__main__":
    main()






















