import re, os
from dateutil.parser import parser

import dummyMapReduce as mr
import sqlite3
import argparse
import sys
import bisect
from pprint import pprint

rxWord = re.compile(r"[a-zA-Z]{3,}")
inverted_db_name = "inverted.db"
create_inverted_db = """CREATE TABLE IF NOT EXISTS Inverted(
                        Term string PRIMARY KEY,
                        Containers blob
                        );"""
dict_for_db = dict()
DOCUMENTS = [
    ("doc1", """MapReduce is a programming model and an associated implementation for processing and generating large
                    data sets with a parallel, distributed algorithm on a cluster."""),
    ("doc2", """Conceptually similar approaches have been very well known since 1995 with the Message Passing
                    Interface standard having reduce and scatter operations."""),
    ("doc3", """A MapReduce program is composed of a Map() procedure (method) that performs filtering and sorting
                (such as sorting students by first name into queues, one queue for each name) and a Reduce() method
                that performs a summary operation (such as counting the number of students in each queue, yielding
                name frequencies)."""),
    ("doc4", """The "MapReduce System" (also called "infrastructure" or "framework") orchestrates the processing by
                marshalling the distributed servers, running the various tasks in parallel, managing all
                communications and data transfers between the various parts of the system, and providing for
                redundancy and fault tolerance."""),
    ("doc5", """The model is inspired by the map and reduce functions commonly used in functional programming,
                although their purpose in the MapReduce framework is not the same as in their original forms."""),
    ("doc6", """The key contributions of the MapReduce framework are not the actual map and reduce functions, but
                the scalability and fault-tolerance achieved for a variety of applications by optimizing the
                execution engine once"""),
    ("doc8", """As such, a single-threaded implementation of MapReduce will usually not be faster than a traditional
                (non-MapReduce) implementation, any gains are usually only seen with multi-threaded implementations"""),
    ("doc9", """The use of this model is beneficial only when the optimized distributed shuffle operation
                (which reduces network communication cost) and fault tolerance features of the MapReduce framework
                come into play."""),
    ("doc10", """Optimizing the communication cost is essential to a good MapReduce algorithm."""),
    ("doc11", "balaur bou balaur balaur balaur balaur")
]


class WordCounter(mr.MapReduce):

    def map(self, _fileName, content):
        print "map:"
        # print " Current document number: ", _fileName
        my_dict = dict()
        words = rxWord.findall(content)
        for idx, word in enumerate(words):
            if word not in my_dict or word.lower() not in my_dict:
                my_dict[word.lower()] = 1
            else:
                my_dict[word.lower()]+= 1
        for a_word in my_dict:
            print "     emiting:::", a_word.lower(), "->", my_dict[a_word]
            self.emit(a_word.lower(), _fileName)#+"->"+str(my_dict[a_word]))

    def reduce(self, word, counts):
        wordCount = counts
        # print word, wordCount
        dict_for_db[word] = wordCount

#################################333
print "main"
wc = WordCounter()
wc.run(DOCUMENTS)