#!/usr/bin/python
import sqlite3
import pefile
import distorm3
import os
import hashlib
import binascii
import bisect
import argparse
import sys
import operator
from pprint import pprint
### SQL Queries

raw_db_name = "features_raw.db"
fine_db_name = "features_fine.db"

create_raw_hw_table = """ CREATE TABLE IF NOT EXISTS Homeworks (
                                    Hash string PRIMARY KEY,
                                    Assign string NOT NULL,
                                    Student string,
                                    Ngrams blob
                                ); """

create_fine_hw_table = """ CREATE TABLE IF NOT EXISTS Homeworks (
                                    Hash string PRIMARY KEY,
                                    Assign string NOT NULL,
                                    Student string,
                                    Ngrams blob
                                ); """


# # 1) Create the database FEATURES_RAW it not exists
with sqlite3.connect(raw_db_name) as conn_raw:
    cursor = conn_raw.cursor()
    cursor.execute(create_raw_hw_table)

# # 2) Create the database FEATURES_FINE if not exists
with sqlite3.connect(fine_db_name) as conn_fine:
    cursor = conn_fine.cursor()
    cursor.execute(create_fine_hw_table)


dict_all_grams_freq = dict();
# # Function that extract n-grames from an exe file
def get_n_grame(filename):
    pe = pefile.PE(filename)
    # An array that holds exectutable sections
    executable_sections_vct=[]
    # An array that holds files containing exectutable sections
    executable_files_vct=[]
    # The exe file contents
    code = open(filename, 'rb')
    # gather hex code of executable sections in some files
    for idx, a_section in enumerate(pe.sections):
        # Get executable sections
        if a_section.__dict__.get('IMAGE_SCN_MEM_EXECUTE')==True:
            executable_sections_vct.append(a_section)
            # Create a file to write the executable section contents
            a_file = open(filename+str(idx)+".txt", "wb")
            # Empty file contents
            a_file.seek(0)
            a_file.truncate()
            # Get offset from file at which section of interest begins
            code.seek(a_section.PointerToRawData)
            # Read SizeOfRawData bytes
            section_contents = code.read(a_section.SizeOfRawData)
            # Write the read data into the file for the section
            a_file.write(section_contents)
            # add the file containing section code to the vector
            executable_files_vct.append(a_file)
            a_file.close()

    for idx, a_section in enumerate(executable_sections_vct):
        # Open file dedicated to executable section
        a_file = open(executable_files_vct[idx].name, 'rb').read()
        # Retrieve assembly code of section 'a_section' from file 'a_file'
        iterable = distorm3.DecodeGenerator(a_section.PointerToRawData, a_file, distorm3.Decode32Bits)
        # Create a list of the OpCodes, with some minor filtering
        list_of_filtered = []
        # elements in the list above declared
        total = 0
        for idx, (offset, size, instruction, hexdump) in enumerate(iterable):
            # Extract OpCode only
            op_code = instruction.split(' ', 1)[0]
            # Skip NOP, INT
            if not "NOP" in op_code and not "INT" in op_code:
                # Transform addition and subtraction operations into '***'
                if "ADD" in op_code or "SUB" in op_code or "INC" in op_code:
                    op_code = "***"
                # Increment the obtained size
                total += 1
                # Add the extracted opcode to the list
                list_of_filtered.append(op_code)
    # I hardcoded 5 to be the size of a gram
    step = 5

    # Crete a set of 5-grames
    n_grames_sorted_list = []

    # Variable to create a a_five_gram string
    a_five_gram = ""
    # N-Grame logic below
    for i in range(0, total-step, 1):
        for j in range(i, i+step, 1):
            a_five_gram+=list_of_filtered[j]
        crc32_of_gram = binascii.crc32(a_five_gram) & 0xffffffff
        #print a_five_gram, "(CRC32: ", crc32_of_gram, ")"
        # five_gram_set.add(a_five_gram)
        if(crc32_of_gram not in n_grames_sorted_list):
            bisect.insort(n_grames_sorted_list, crc32_of_gram)
            if(crc32_of_gram not in dict_all_grams_freq):
                dict_all_grams_freq[crc32_of_gram] = 1
            else:
                dict_all_grams_freq[crc32_of_gram] += 1

        a_five_gram = ""
    # Clean disk of files created in this func
    for a_file in executable_files_vct:
        os.remove(a_file.name)

    #for i in range(0, n_grames_sorted_list.__len__()):
    #    print n_grames_sorted_list[i]
    return n_grames_sorted_list

# Function to hash an entire file using MD5
def md5(filename):
    hash_md5 = hashlib.md5()
    with open(filename, "rb") as f:
        for chunk in iter(lambda : f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()

### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###

parser = argparse.ArgumentParser(description="lab5 BIG data")
parser.add_argument ("-mkdbraw", action='store_true', help="SQL:populate a raw AF hw database", required=False)
parser.add_argument ("-finedbmk", action='store_true', help="SQL:populate a FINE AF hw database", required=False)
parser.add_argument ("-task34", action='store_true', help="Jaccard on two hashes", required=False)
args = parser.parse_args()
if len(sys.argv[1:])==0:
    parser.print_help()

        # PROGRAM BEGINS HERE #

### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###
### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###
if args.mkdbraw:
    # os.remove(fine_db_name)
    # os.remove(raw_db_name)
    ####### Calculate 5-gram on all exe files provided
    number_of_files = 0
    for filename in os.listdir('./homeworks/binaries'):
        number_of_files += 1

    # Process the exe files
    for idx,filename in enumerate(os.listdir('./homeworks/binaries')):
        n_grames_list = get_n_grame("./homeworks/binaries/"+filename)
        s_hash_value = str(md5("./homeworks/binaries/"+(filename)))
        s_assignment_number = str(int(filename[1:3]))
        s_student_number = str(int(filename[5:9]))

        with open('temp.temp', 'wb') as pickled_blob:
            for a_gram in n_grames_list:
                pickled_blob.write(str(a_gram)+"\n")
        query_insert_features_raw = '''INSERT INTO Homeworks (Hash, Assign, Student, Ngrams) values\
                                    (?, ?, ?, ?);'''
        with open('temp.temp', 'rb') as pickled_blob:
            ablob = pickled_blob.read()
        conn_raw.execute(query_insert_features_raw, \
                         [s_hash_value,\
                       s_assignment_number,\
                       s_student_number,\
                       buffer(ablob)])
        os.remove("temp.temp")
        conn_raw.commit()

        print filename, "student#", s_student_number, "assignment#", s_assignment_number, "[", s_hash_value, "]"
        print idx, "/",number_of_files

    pprint(dict_all_grams_freq)
    with open('occurencies.txt', 'wb') as file_with_count_dict:
        for key, value in dict_all_grams_freq.items():
            file_with_count_dict.write(str(key)+":"+str(value)+"\n")
### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###
### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###



### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###
### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###
if args.finedbmk:
    # The dictionary with number of occurences for each crc32
    count_dict = dict()
    # Parse the file and place data into the dictionary
    with open('occurencies.txt', 'rb') as file_with_count_dict:
        for line in file_with_count_dict:
            # print line.split(':')[0], "->", line.split(':')[1]
            count_dict[line.split(':')[0]] = line.split(':')[1][:-1]
    # pprint(count_dict)

    query_get_all_blos = "SELECT * from Homeworks"
    with sqlite3.connect(raw_db_name) as conn_raw:
        cursor = conn_raw.cursor()
        cursor.execute(query_get_all_blos)
        cnt_test = 0
        for idx, row in enumerate(cursor):
            list_of_files_grams = []
            # Each of these is a blob stored in DB previously
            cnt_test+=1
            # print row[0]
            list_of_files_grams = str(row[3]).split('\x0a')
            print "#",  idx, "LEN Before:", len(list_of_files_grams),
            for an_elem in list_of_files_grams:
                if an_elem != '':
                    if count_dict[an_elem] > 30:
                        # print "ELIMINATING ", an_elem, " which had: ",count_dict[an_elem]
                        list_of_files_grams.remove(an_elem)
            print "LEN After:", len(list_of_files_grams)

            with open('temp.temp', 'wb') as pickled_blob:
                for a_gram in list_of_files_grams:
                    pickled_blob.write(str(a_gram) + "\n")
            a_query = query_insert_features_raw = '''INSERT INTO Homeworks (Hash, Assign, Student, Ngrams) values\
                                    (?, ?, ?, ?);'''

            with open('temp.temp', 'rb') as pickled_blob:
                ablob = pickled_blob.read()

            print "PUTING IN DB"

            conn_fine.execute(a_query, \
                             [row[0], \
                              row[1], \
                              row[2], \
                              buffer(ablob)])
            conn_fine.commit()
### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###
### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###

######################################
def sequel_blob_to_list(blob):
    the_list = []
    the_list = str(blob).split('\x0a')
    the_list = the_list[:-2]
    return the_list
######################################

#########################################
def intersection_of_ordered_lists(list1, list2):
    int_list = []
    sz1, sz2 = len(list1), len(list2)
    i, j = 0, 0
    while i < sz1 and j < sz2:
        if list1[i] < list2[j]:
            i+=1
        else:
            if list2[j] < list1[i]:
                j+=1
            else:
                int_list.append(list2[j])
                i += 1
                j += 1
    return int_list
#########################################

#########################################
def union_of_ordered_lists(list1, list2):
    union_list = []
    sz1, sz2 = len(list1), len(list2)
    i, j = 0, 0
    while i < sz1 and j < sz2:
        if list1[i] < list2[j]:
            union_list.append(list1[i])
            i+=1
        else:
            if list2[j] < list1[i]:
                union_list.append(list2[j])
                j+=1
            else:
                union_list.append(list2[j])
                i+=1
                j+=1
    return union_list
#########################################


# # # # # TASK 3 # # # # #
def sim1(db, h1, h2):
    first_students_ngrams = []
    second_student_ngrams = []
    with sqlite3.connect(db) as conn_fine:

        query1 = "SELECT * from Homeworks where Hash='"+h1+"'"
        cursor_fine = conn_fine.cursor()
        cursor_fine.execute(query1)
        for row in cursor_fine:
            first_students_ngrams = sequel_blob_to_list(row[3])

        query2 = "SELECT * from Homeworks where Hash='"+h2+"'"
        cursor_fine.execute(query2)
        for row in cursor_fine:
            second_student_ngrams = sequel_blob_to_list(row[3])

        # pprint(len(first_students_ngrams))
        # pprint(len(second_student_ngrams))
        inters_list = intersection_of_ordered_lists(first_students_ngrams, second_student_ngrams)
        union_list = union_of_ordered_lists(first_students_ngrams, second_student_ngrams)
        jaccard = float(len(inters_list))/len(union_list)
    return jaccard
# # # # # TASK 3 # # # # #

# # # # # # TASK 3.1 # # # # #
def sim2(db, assign, s1, s2):
    first_students_ngrams = []
    second_students_ngrams = []
    with sqlite3.connect(db) as conn_fine:
        query1 = "SELECT * from Homeworks where Assign='"+str(assign)+"' and Student='"+str(s1)+"'"
        # print query1
        cursor_fine = conn_fine.cursor()
        cursor_fine.execute(query1)
        for row in cursor_fine:
            first_students_ngrams = sequel_blob_to_list(row[3])
            # pprint(first_students_ngrams)

        query2 = "SELECT * from Homeworks where Assign='"+str(assign)+"' and Student='"+str(s2)+"'"
        # print query2
        cursor_fine = conn_fine.cursor()
        cursor_fine.execute(query2)

        for row in cursor_fine:
            second_students_ngrams = sequel_blob_to_list(row[3])
            # pprint(second_students_ngrams)

        inters_list = intersection_of_ordered_lists(first_students_ngrams, second_students_ngrams)
        union_list = union_of_ordered_lists(first_students_ngrams, second_students_ngrams)
        # print len(first_students_ngrams), "::", len(second_students_ngrams)
        jaccard = float(len(inters_list))/len(union_list)
    return jaccard
# # # # # # TASK 3.1 # # # # #

# # # # # # # # TASK 4 # # # ## # # # # # # #
def task4(db, assign):
    # dict_of_all_jacards = dict()
    list_of_assignments = []
    dict_stud_grams = dict()

    with sqlite3.connect(db) as conn_fine:
        q_get_all_assign_numbers = "SELECT distinct Assign FROM Homeworks ORDER by Assign"
        cursor_fine = conn_fine.cursor()
        cursor_fine.execute(q_get_all_assign_numbers)
        for a_row in cursor_fine:
            list_of_assignments.append(a_row[0])
        pprint(list_of_assignments)
        for idx in range(0, len(list_of_assignments), 1):
            list_of_students = []
            q_get_by_assign = "\nSELECT * from Homeworks where Assign ='"+str(list_of_assignments[idx])+"'"
            print q_get_by_assign
            cursor_fine = conn_fine.cursor()
            cursor_fine.execute(q_get_by_assign)
            for a_row in cursor_fine:
                student = a_row[2]
                bisect.insort(list_of_students, student)
                students_grams = sequel_blob_to_list(a_row[3])
                dict_stud_grams[student] = students_grams
            # pprint(list_of_students)

            lennn = len(list_of_students)
            print "Asign#", list_of_assignments[idx], ", ", lennn

            jaccard = 0
            for i in range(0, lennn, 1):
                grams_of_first = dict_stud_grams[list_of_students[i]]
                for j in range(i+1, lennn, 1):
                    grams_of_second = dict_stud_grams[list_of_students[j]]
                    jac_num = len(intersection_of_ordered_lists(grams_of_first,grams_of_second))
                    jac_den = len(union_of_ordered_lists(grams_of_first, grams_of_second))
                    if (0!=jac_den):
                        jaccard1 = (float(jac_num) / jac_den)
                        jaccard = format((float(jac_num) / jac_den), '.4f')
                        if jaccard1 > 0.45 :
                            print " [HW#"+str(list_of_assignments[idx])+"]Jacc["+str(list_of_students[i])+\
                                  "]["+str(list_of_students[j])+"]=", jaccard



# # # # # # # # TASK 4 # # # ## # # # # # # #


if args.task34:

    # jacard = sim1("features_fine.db", "dc9cad90d4bf7c99e257dbc3c5dce0f2", "0db7861e5588fbb853b2d29ff8837cac")
    # print jacard

    # jaccard1 = sim2("features_fine.db", 5, 22, 8)
    # print jaccard1

    task4("features_fine.db", 5)







