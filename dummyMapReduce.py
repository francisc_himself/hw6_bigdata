
class MapReduce:
    # Constructor: initialize a dict called data
    def __init__(self):
        self.data = {}

    def run(self, collection):
        # Empth the field data
        self.data = {}
        for key, values in collection:
            # Perform map on key-value pairs
            self.map(key, values)
        for key, values in self.data.iteritems():
            # Next, perform reduce on key-value pairs
            self.reduce(key, values)

    def emit(self, key, value):
        # Emit checks wether the key-value pair exists
        # in the data field, and adds non-existing one
        if key in self.data:
            self.data[key].append(value)
        else:
            self.data[key] = [value]

    def map(self, _key, _values):
        raise Exception("map function not implemented")

    def reduce(self, _key, _values):
        raise Exception("reduce function not implemented")